This is a demo program using arduino-copilot. 

To build the C code:

	stack build arduino-copilot
	stack runghc Demo.hs

The resulting `.ino` sketch can be loaded into the Arduino IDE and flashed
to an Arduino Uno board using the IDE.

## Arduino-Makefile integration

The `Makefile` and `pre-build-hook.sh` show how to integrate arduino-copilot
with <https://github.com/sudar/Arduino-Makefile>. This automates generating
the C code, compiling that, and flashing it onto an Arduino Uno board,
with a single command

	make upload

Note that you will need to manually build the C code once, as shown above,
since the Arduino-Makefile expects to find a `.ino` file.

The path in the `Makefile` to `Arduino.mk` may need to be adjusted (the
default is the location where on a Debian system, `apt-get install
arduino-mk` will install it).
