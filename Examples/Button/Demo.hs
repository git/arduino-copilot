{-# LANGUAGE RebindableSyntax #-}

module Examples.Button.Demo where

import Copilot.Arduino.Uno

main :: IO ()
main = arduino $ do
	buttonpressed <- input' pin12 [False, False, False, True, True]
	led =: buttonpressed || blinking
	delay =: MilliSeconds (longer_and_longer * 2)

longer_and_longer :: Stream Word32
longer_and_longer = counter $ counter false `mod` 64 == 0

counter :: Stream Bool -> Stream Word32
counter reset = s
   where
	s = if reset then 0 else z + 1
	z = [0] ++ s

