Arduino programming in haskell using the Copilot stream DSL

See Copilot.Arduino for details on how to use write a program using this
library.

For example, to make an Arduino blink its LED:

	import Copilot.Arduino
	main = arduino $ do
		led =: blinking
		delay =: MilliSeconds (constant 100)

This and other examples are included in the Examples/ directory, each
with their own README explaining how to build and use them.

## Contributing

Contributions are welcome, including adding support for more
Arduino C libraries, provided that the C library is reasonably commonly
used, and is licensed with a free software license.

Contributors of such libraries will be expected to maintain them,
otherwise they may get removed if they are blocking changes to
arduino-copilot or are buggy.

Any contribution should have well documented functions and types.
