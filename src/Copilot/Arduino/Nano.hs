-- | Programming the Arduino Nano with Copilot.

{-# LANGUAGE DataKinds #-}

module Copilot.Arduino.Nano (
	module Copilot.Arduino
	-- * Pins
	, pin2
	, pin3
	, pin4
	, pin5
	, pin6
	, pin7
	, pin8
	, pin9
	, pin10
	, pin11
	, pin12
	, pin13
	, a0
	, a1
	, a2
	, a3
	, a4
	, a5
	, a6
	, a7
	, sizeOfEEPROM
) where

import Copilot.Arduino
import Copilot.Arduino.Internals

pin2, pin4, pin7, pin8, pin12, pin13 :: Pin '[ 'DigitalIO ]
pin3, pin5, pin6, pin9, pin10, pin11 :: Pin '[ 'DigitalIO, 'PWM ]
pin2 = Pin (Arduino 2)
pin3 = Pin (Arduino 3)
pin4 = Pin (Arduino 4)
pin5 = Pin (Arduino 5)
pin6 = Pin (Arduino 6)
pin7 = Pin (Arduino 7)
pin8 = Pin (Arduino 8)
pin9 = Pin (Arduino 9)
pin10 = Pin (Arduino 10)
pin11 = Pin (Arduino 11)
pin12 = Pin (Arduino 12)
-- | This pin is connected to the `led`
pin13 = Pin (Arduino 13)

a0, a1, a2, a3, a4, a5 :: Pin '[ 'AnalogInput, 'DigitalIO ]
a0 = Pin (Arduino 14)
a1 = Pin (Arduino 15)
a2 = Pin (Arduino 16)
a3 = Pin (Arduino 17)
a4 = Pin (Arduino 18)
a5 = Pin (Arduino 19)

-- | Limited to analog input.
a6, a7 :: Pin '[ 'AnalogInput ]
a6 = Pin (Arduino 20)
a7 = Pin (Arduino 21)

-- | Different versions of Arduino Nano have different EEPROM sizes.
-- This is a safe value that will work on all versions.
sizeOfEEPROM :: Word16
sizeOfEEPROM = 512
